const taskInput = document.querySelector('#taskInput');
const addTaskBtn = document.querySelector('#addTaskBtn');
const taskList = document.querySelector('#taskList');
const clearTaskBtn = document.querySelector('#clearTaskBtn');

document.addEventListener('DOMContentLoaded', () => {
    const savedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
    savedTasks.forEach(task => addTaskToDOM(task));
});

addTaskBtn.addEventListener('click', addTask);
taskList.addEventListener('click', handleTaskClick);
clearTaskBtn.addEventListener('click', clearTaskList);

function addTask() {
    const taskText = taskInput.value.trim();

    if (taskText !== '') {
        const task = {
            text: taskText,
            completed: false
        };

        addTaskToDOM(task);
        saveTasksToLocalStorage(task);
        taskInput.value = '';
    }
}

function addTaskToDOM(task) {
    const li = document.createElement('li');
    li.classList.add('task');
    li.innerHTML = `
        <input type="checkbox">
        <span class="task-text">${task.text}</span>
        <button class="delete-btn"><i class="fas fa-trash-alt"></i></button>
    `;

    const checkbox = li.querySelector('input[type="checkbox"]');
    checkbox.checked = task.completed;
    if (task.completed) {
        li.querySelector('.task-text').style.textDecoration = 'line-through';
    }

    taskList.appendChild(li);
}

function handleTaskClick(event) {
    const target = event.target;

    if (target.classList.contains('delete-btn')) {
        const li = target.closest('.task');
        const taskText = li.querySelector('.task-text').textContent;
        removeTaskFromLocalStorage(taskText);
        li.remove();
    } else if (target.tagName === 'INPUT' && target.type === 'checkbox') {
        const li = target.closest('.task');
        const taskText = li.querySelector('.task-text').textContent;
        const savedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
        const updatedTasks = savedTasks.map(task => {
            if (task.text === taskText) {
                task.completed = !task.completed;
            }
            return task;
        });
        localStorage.setItem('tasks', JSON.stringify(updatedTasks));
        if (target.checked) {
            li.querySelector('.task-text').style.textDecoration = 'line-through';
        } else {
            li.querySelector('.task-text').style.textDecoration = 'none';
        }
    }
}

function saveTasksToLocalStorage(task) {
    const savedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
    savedTasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(savedTasks));
}

function removeTaskFromLocalStorage(taskText) {
    let savedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
    savedTasks = savedTasks.filter(task => task.text !== taskText);
    localStorage.setItem('tasks', JSON.stringify(savedTasks));
}

function clearTaskList() {
    taskList.innerHTML = '';
    localStorage.removeItem('tasks');
}
