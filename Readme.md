# Lista de Tarefas

Este projeto é uma aplicação web simples de lista de tarefas. Você pode adicionar, marcar como concluídas e excluir tarefas, bem como limpar toda a lista. As tarefas são salvas no localStorage do navegador, para que você possa recarregar a página sem perder seus dados.

## Funcionalidades

- Adicionar novas tarefas
- Marcar tarefas como concluídas
- Excluir tarefas
- Limpar toda a lista de tarefas
- Salvamento automático das tarefas no localStorage

## Tecnologias Utilizadas

- HTML
- CSS
- JavaScript

## Licença

Este projeto está licenciado sob a Licença BSD 3-Clause.
